#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include "pidinfo.h"

/* Part I */
void run_current()
{
    int pid = 0;

    /* Your code here. */
	//Opens a write-able Filestream to get_current() in kernelspace.
	FILE *file = fopen("/sys/kernel/kernellab/get_current()", "w");
	//Prints error msg if filestream is unable to open and exits.
	if(!file){
		perror("fopen error");
		exit(1);
	}	
	//Writes current pid to get_current().
	fprintf(file, "%u\n", &pid);
	//Flushes and closes filestream.
    fflush(file);    
	fclose(file);

    printf("Current PID: %d\n", pid);
}

/* Part II */
void run_pid()
{
	//Creates a pid_info struct to be able to write msg to kernel space.
    struct pid_info info;

	 // Copy the info to our info struct.
    struct sysfs_message message;
    message.pid = getpid();
    message.address = &info;

	//Opens a write-able Filestream to pid in kernelspace.
    FILE *file = fopen("/sys/kernel/kernellab/pid", "w");
	//Prints error msg if filestream is unable to open and exits.
    if(!file){
        perror("fopen error");
        exit(1);
    } 
	//Writes message to pid file.
    fprintf(file, "%u\n", &message);
	//Flushes and closes filestream.
    fflush(file);    
    fclose(file);

	//Prints info on proccess to the console.
    printf("PID: %d\n", info.pid);
    printf("COMM: %s\n", info.comm);
    printf("State: %ld\n", info.state);
}


int main()
{
    run_current();    
    run_pid();
    return EXIT_SUCCESS;
}
