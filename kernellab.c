/*
 * Kernellab
 */
#include <linux/types.h>
#include <linux/sched.h>
#include <linux/uaccess.h>
#include <linux/kernel.h>
#include <linux/kobject.h>
#include <linux/string.h>
#include <linux/sysfs.h>
#include <linux/module.h>
#include <linux/init.h>

#include "pidinfo.h"

/* Part I */
static ssize_t current_write(struct kobject *kobj, struct kobj_attribute *attr,
             const char *buf, size_t count)
{

    unsigned int user_addr;
    sscanf(buf, "%du", &user_addr);

    int pid = current->pid;
    copy_to_user(user_addr, &pid, sizeof(pid));
    
    return count;
}

static struct kobj_attribute current_attribute =
    __ATTR(current, 0222, NULL, current_write);


/* Part II */
static ssize_t pid_write(struct kobject *kobj, struct kobj_attribute *attr,
             const char *buf, size_t count)
{
    // Stores the message from program.
    struct sysfs_message message;
    // Stores the pid_info struct to copy to user.
    struct pid_info info;
    // Stores the address of the message in userspace.
    unsigned int user_addr;
    struct task_struct *task_list;

    // Get the address of the sysfs_message in userspace.
    sscanf(buf, "%du", &user_addr);
    
    // Copy the message to here.
    copy_from_user(&message, user_addr, sizeof(message));

    // Find the right process.
    for_each_process(task_list) {
        if (task_list->pid == message.pid)
        {
            // Copy the info to our info struct.
            info.pid = message.pid;
            strncpy(info.comm, task_list->comm, 16);
            info.state = task_list->state;
            break;
        }
    }

    // Override the userspace info struct.
    copy_to_user(message.address, &info, sizeof(info));

    return count;
}

static struct kobj_attribute pid_attribute =
    __ATTR(pid, 0222, NULL, pid_write);

/* Setup  */
static struct attribute *attrs[] = {
    &current_attribute.attr,
    &pid_attribute.attr,
    NULL,
};
static struct attribute_group attr_group = {
    .attrs = attrs,
};

static struct kobject *kernellab_kobj;

static int __init kernellab_init(void)
{
    struct task_struct *task_list;

    /* Your code here. */
    printk("kernellab module INJECTED\n");

    for_each_process(task_list) {

        if (task_list->pid == 1)
        {
            printk("%s\n", task_list->comm);
            break;
        }
    }

    
    int retval;
    kernellab_kobj = kobject_create_and_add("kernellab", kernel_kobj);
    if (!kernellab_kobj)
        return -ENOMEM;

    retval = sysfs_create_group(kernellab_kobj, &attr_group);
    if (retval) 
        kobject_put(kernellab_kobj);
    return retval;
}

static void __exit kernellab_exit(void)

{
    /* Your code here. */
    printk("kernellab module UNLOADED\n");
    kobject_put(kernellab_kobj);
}

module_init(kernellab_init);
module_exit(kernellab_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Bergsteinn Karlsson <bergsteinn12@ru.is");
MODULE_AUTHOR("Halldor Sigurdsson <halldor12@ru.is");
